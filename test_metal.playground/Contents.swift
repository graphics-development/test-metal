import PlaygroundSupport
import MetalKit

guard let device = MTLCreateSystemDefaultDevice() else {
    fatalError("GPU is not supported!")
}

let frame = CGRect(x: 0, y: 0, width: 600, height: 600)
let view = MTKView(frame: frame, device: device)

view.clearColor = MTLClearColor(red: 1, green: 1, blue: 0.0, alpha: 1)

guard let metalCommandQueue = device.makeCommandQueue() else {
    fatalError("Couldn't create a command queue!")
}

let metalShaders = """
#include <metal_stdlib>
using namespace metal;

struct VertexIn {
    float3 position [[attribute(0)]];
};

struct VertexOut {
    float4 position [[position]];
};

vertex VertexOut vertexShader(VertexIn in [[stage_in]]) {
    VertexOut out;
    out.position = float4(in.position, 1.0);
    return out;
};

fragment float4 fragmentShader() {
    return float4(1.0, 0.0, 0.0, 1.0);
};

"""

let vertices = [
    SIMD3<Float>(-1.0, -1.0, 0.0),
    SIMD3<Float>(1.0, -1.0, 0.0),
    SIMD3<Float>(0.0, 1.0, 0.0)
]

let vertexBuffer = device.makeBuffer(bytes: vertices, length: MemoryLayout<SIMD3<Float>>.stride * vertices.count, options: [])

let library = try device.makeLibrary(source: metalShaders, options: nil)
      let vertexFunction = library.makeFunction(name: "vertexShader")
      let fragmentFunction = library.makeFunction(name: "fragmentShader")

let vertexDesciptor = MTLVertexDescriptor()
vertexDesciptor.attributes[0].format = .float3
vertexDesciptor.attributes[0].offset = 0
vertexDesciptor.attributes[0].bufferIndex = 0

vertexDesciptor.layouts[0].stride = MemoryLayout<SIMD3<Float>>.stride
vertexDesciptor.layouts[0].stepRate = 1
vertexDesciptor.layouts[0].stepFunction = .perVertex

let pipelineDescriptor = MTLRenderPipelineDescriptor()
pipelineDescriptor.vertexDescriptor = vertexDesciptor
pipelineDescriptor.vertexFunction = vertexFunction
pipelineDescriptor.fragmentFunction = fragmentFunction
pipelineDescriptor.colorAttachments[0].pixelFormat = .bgra8Unorm

let pipelineState = try device.makeRenderPipelineState(descriptor: pipelineDescriptor)

guard let commandBuffer = metalCommandQueue.makeCommandBuffer(),
  let renderPassDescriptor = view.currentRenderPassDescriptor,
  let renderEncoder = commandBuffer.makeRenderCommandEncoder(
    descriptor: renderPassDescriptor)
else { fatalError("Render Error!") }

renderEncoder.setRenderPipelineState(pipelineState)
renderEncoder.setVertexBuffer(vertexBuffer, offset: 0, index: 0)
renderEncoder.drawPrimitives(type: .triangle, vertexStart: 0, vertexCount: vertices.count)

renderEncoder.endEncoding()

guard let drawable = view.currentDrawable else {
  fatalError("Error!")
}

commandBuffer.present(drawable)
commandBuffer.commit()

PlaygroundPage.current.liveView = view
